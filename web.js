const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');

module.exports = (env) => {
    const devMode = !env.production;
    return {
        mode: devMode ? 'development' : 'production',
        entry: './src/app/index.ts',
        output: {
            path: path.resolve(__dirname, 'dist'),
            filename: 'app.bundle.js',
            clean: true,
        },
        devtool: devMode ? 'source-map' : false,
        resolve: {
            extensions: [".ts", ".tsx", ".js"],
            plugins: [new TsconfigPathsPlugin({})]
        },
        module: {
            rules: [
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        "style-loader",
                        {
                            loader: "css-loader",
                            options: {
                                sourceMap: devMode,
                            },
                        },
                        {
                            loader: "sass-loader",
                            options: {
                                implementation: require("sass"),
                                sourceMap: devMode,
                            },
                        },
                    ],
                },
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                }
            ],
        },
        plugins: [
            new HtmlWebpackPlugin(),
        ],
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000,
        },
    };
};
