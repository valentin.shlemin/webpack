export function meter(func, meterParams = {count: 1, name: 'Test'}) { //default params и связаные с ними conts сокращены
    return (...params) => {
        let resultFunc;
        const timerArray = [];
        while (timerArray.length < meterParams.count) {
            const timeStart = performance.now(); 
            resultFunc = func.apply(params); //сокращен контекст
            timerArray.push((performance.now()- timeStart).toFixed(3)); //сокращено const timeEnd 
        }
        
        const medianValue = timerArray.sort((a, b) => { //сокращено const sortedArray
            return (a>b) ? 1 : (a<b) ? -1 : 0})[Math.floor(timerArray.length / 2)]; //сокращение тернарником 

        console.log (`%c ${meterParams.name} (count ${meterParams.count}): ${medianValue}ms`, 'color:#77ff33'); 
        
        return resultFunc;
    };
}
