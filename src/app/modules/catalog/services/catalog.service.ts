//import {CoreModule} from '$core/core.module';
import {AbstractService} from '$helpers/abstract.service';
import {IServiceParams} from '$interfaces/index';

export class CatalogService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'CatalogService',
        inject: [],
    }

    constructor() {
        super();
    }
}
