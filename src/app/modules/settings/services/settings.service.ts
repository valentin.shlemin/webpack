//import {CoreModule} from '$core/core.module';
import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces/index';

export class SettingsService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'SettingsService',
        inject: [],
    }

    protected catalogService: SettingsService;

    constructor() {
        super();
    }

    public init(bootstrap: any):void { 
        super.init(bootstrap);
    }
}
