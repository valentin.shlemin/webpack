import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces/index';

import {CatalogService} from '$catalog/index';
import {ReaderService} from '$reader/index';
import {SettingsService} from '$settings/index';
import {StatusbarService} from '$statusbar/index';

export class CompilerService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'CompilerService',
        inject: [
            'Catalog.CatalogService',
            'Reader.ReaderService',
            'Settings.SettingsService',
            'Statusbar.StatusbarService',
        ],
    }

    protected catalogService: CatalogService;
    protected readerService: ReaderService;
    protected settingsService: SettingsService;
    protected statusbarService: StatusbarService;

    constructor() {
        super();
    }

    public init(bootstrap: any):void {
        super.init(bootstrap);
        // console.log('after', this.catalogService);
        // console.log('after', this.readerService);
        // console.log('after', this.settingsService);
        // console.log('after', this.statusbarService);
    }
}
