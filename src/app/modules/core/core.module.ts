import {AbstractModule} from '$helpers/index';
import {CompilerService} from '$core/services';

export class CoreModule extends AbstractModule {
    public readonly moduleParams = {
        name: 'Core',
        components: [],
        providers: [
            CompilerService,
        ],
    }

    constructor() {
        super();
    }
}
