//import {CoreModule} from '$core/core.module';
import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces/index';

export class ReaderService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'ReaderService',
        inject: [],
    }

    protected catalogService: ReaderService;

    constructor() {
        super();
    }

    public init(bootstrap: any):void { 
        super.init(bootstrap);
    }
}
