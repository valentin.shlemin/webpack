import {CoreModule} from '$core/core.module';
import {CatalogModule} from '$catalog/index';
import {ReaderModule} from '$reader/index';
import {SettingsModule} from '$settings/index';
import {StatusbarModule} from '$statusbar/index';

const modules = [
    CoreModule,
    CatalogModule,
    ReaderModule,
    SettingsModule,
    StatusbarModule
];

export interface IProviders {
    [key: string]: any;
}

export interface IModuleItem {
    instance: any;
    providers: IProviders;
}

export interface IModules {
    [key: string]: IModuleItem;
}

export class Bootstrap{
    private _modules: IModules = {};

    constructor() {
        modules.forEach((module:any) => {
            this.startModule(module);
        });
        this.initServices(); 
        //console.log(this._modules); 
    }

    public getServiceInstance<T extends any>(name: string): T {
        const splitName = name.split('.');
        return this._modules[splitName[0]].providers[splitName[1]];
    }

    private startModule(module:any): void {
        const moduleInstance = new module(),
            providersList = moduleInstance.moduleParams.providers || [],
            providers: IProviders = {};
        providersList.forEach((provider:any):void => {
            const providerInstance = new provider();
            providers[providerInstance.serviceParams.name] = providerInstance;
        });
        this._modules[moduleInstance.moduleParams.name] = {
            instance: moduleInstance,
            providers
        };
    }

    private initServices(): void {
        Object.keys(this._modules).forEach((key:string):void => {
            Object.keys(this._modules[key].providers).forEach((keyService: string): void => {
                this._modules[key].providers[keyService].init(this);
            });
        });
    }
}
