//import {CoreModule} from '$core/core.module';
import {AbstractService} from '$helpers/index';
import {IServiceParams} from '$interfaces/index';

export class StatusbarService extends AbstractService {
    public readonly serviceParams: IServiceParams = {
        name: 'StatusbarService',
        inject: [],
    }

    protected catalogService: StatusbarService;

    constructor() {
        super();
    }

    public init(bootstrap: any):void { 
        super.init(bootstrap);
    }
}
